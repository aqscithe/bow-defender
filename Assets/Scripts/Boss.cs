﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    // config params

    [Header("Enemy")]
    [SerializeField] float health = 1000f;
    [SerializeField] int scoreValue = 100;
    [SerializeField] GameObject explosionPrefab;

    [Header("Projectile")]
    float shotCounter;
    [SerializeField] float minTimeBetweenShots = 0.2f;
    [SerializeField] float maxTimeBetweenShots = 3f;
    [SerializeField] float projectileSpeed = 5f;
    [SerializeField] GameObject projectilePrefab;

    [Header("Audio")]
    [SerializeField] AudioClip explosionClip;
    [Range(0f, 1f)] [SerializeField] float explosionVolume = 0.2f;


    // cached reference
    GameStatus gameStatus;

    // Start is called before the first frame update
    void Start()
    {
        RandomizeShotCounter();
    }

    // Update is called once per frame
    void Update()
    {
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0)
        {
            Fire();
            RandomizeShotCounter();
        }
    }

    private void RandomizeShotCounter()
    {
        shotCounter = UnityEngine.Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    private void Fire()
    {
        GameObject firedProjectile = Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
        firedProjectile.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -projectileSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if (!damageDealer) { return; }
        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        FindObjectOfType<GameStatus>().IncreaseScore(scoreValue);
        GameObject explosion = Instantiate(explosionPrefab, transform.position, explosionPrefab.transform.rotation);
        Destroy(gameObject);
        AudioSource.PlayClipAtPoint(explosionClip, Camera.main.transform.position, explosionVolume);
    }
}
