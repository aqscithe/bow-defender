﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPathing : MonoBehaviour
{
    [SerializeField] int startLoopWaypointIndex = 0;

    // cached reference
    WaveConfig waveConfig;

    float enemySpeed;
    List<Transform> waypoints;
    int waypointIndex = 0;


    // Start is called before the first frame update
    void Start()
    {
        enemySpeed = waveConfig.GetEnemySpeed();
        waypoints = waveConfig.GetWaypoints();
    }


    // Update is called once per frame
    void Update()
    {
        MoveEnemy();
    }

    private void MoveEnemy()
    {

        if (waypointIndex < waypoints.Count)
        {
            MoveToNextWaypoint();

        }
        else if(gameObject)
        {
            waypointIndex = startLoopWaypointIndex;
            MoveToNextWaypoint();
        }

    }

    public void SetWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    private void MoveToNextWaypoint()
    {
        var targetPosition = waypoints[waypointIndex].transform.position;
        var movementThisFrame = enemySpeed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementThisFrame);

        if (transform.position == targetPosition)
        {
            waypointIndex++;
        }
    }
}
