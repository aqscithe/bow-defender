﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // config params
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] int startingWave = 0;
    [SerializeField] bool looping = false;
    [SerializeField] int wavesCompleted; // serialized for debugging
    [SerializeField] int wavesToBeat = 8;

    GameObject enemyPrefab;
    List<Transform> waypoints;
    float spawnRate;
    int enemyCount;
    

    // Start is called before the first frame update
    IEnumerator Start()
    {
        wavesCompleted = 0;

        do
        {
            yield return StartCoroutine(SpawnAllWaves(waveConfigs));
        }
        while (looping);
    }

    private void AddToWavesCompleted()
    {
        wavesCompleted++;
    }

    public int GetWavesCompleted()
    {
        return wavesCompleted;
    }

    public int GetWavesToBeat()
    {
        return wavesToBeat;
    }

    public List<WaveConfig> GetWaveConfigs()
    {
        return waveConfigs;
    }

    IEnumerator SpawnAllWaves(List<WaveConfig> waveConfigs)
    {
        for (int waveIndex = startingWave; waveIndex < waveConfigs.Count; waveIndex++)
        {
            if(wavesCompleted >= wavesToBeat)
            {
                FindObjectOfType<Level>().OnLastWave();
                break;
            }
            var currentWave = waveConfigs[waveIndex];
            yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));
            AddToWavesCompleted();
        }
    }

    IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
    {
        enemyPrefab = waveConfig.GetEnemyPrefab();
        waypoints = waveConfig.GetWaypoints();
        spawnRate = waveConfig.GetEnemySpawnRate();
        enemyCount = waveConfig.GetEnemyCount();
        for(int enemyIndex = 0; enemyIndex < enemyCount; enemyIndex++)
        {
            GameObject enemySpawn = Instantiate(enemyPrefab, waypoints[0].transform.position, enemyPrefab.transform.rotation);
            enemySpawn.GetComponent<EnemyPathing>().SetWaveConfig(waveConfig);
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
