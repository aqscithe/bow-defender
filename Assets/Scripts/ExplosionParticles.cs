﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParticles : MonoBehaviour
{
    // config params
    [SerializeField] float timeTillDestroy = 1f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, timeTillDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
