﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvas : MonoBehaviour
{
    private void Awake()
    {
        SetUpSingleton();
    }

    public void ResetGameCanvas()
    {
        Destroy(gameObject);
    }

    private void SetUpSingleton()
    {
        Debug.Log("Hello");
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
