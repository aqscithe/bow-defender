﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameStatus : MonoBehaviour
{
    [SerializeField] int score; // serialized for debugging purposes
    [SerializeField] int scoreThisLevel; // serialized for debugging purposes
    [SerializeField] AudioClip powerupClip;
    [SerializeField] [Range(0.1f, 1f)] float powerupVolume = 0.5f;


    bool healthPowerupAvailable = true;

    private void Awake()
    {
        SetUpSingleton();
    }

    void Start()
    {
        score = 0;
        scoreThisLevel = 0;
    }

    public void IncreaseScore(int scoreValue)
    {
        score += scoreValue;
        scoreThisLevel += scoreValue;
        if(SceneManager.GetActiveScene().name != "Boss")
        {
            if (healthPowerupAvailable)
            {
                CheckPowerupPoints();
            }
        }
        
    }

    private void CheckPowerupPoints()
    {
        if (scoreThisLevel >= FindObjectOfType<PowerupDispenser>().GetPowerupPoints())
        {
            FindObjectOfType<Player>().AddBonusHealth();
            AudioSource.PlayClipAtPoint(powerupClip, Camera.main.transform.position, powerupVolume);
            healthPowerupAvailable = false;
        }
    }

    public void ResetLevelScore()
    {
        healthPowerupAvailable = true;
        scoreThisLevel = 0;
    }

    public int GetScore()
    {
        return score;
    }

    public void ResetGameStatus()
    {
        Destroy(gameObject);
    }

    private void SetUpSingleton()
    {
        Debug.Log("Hello");
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

}
