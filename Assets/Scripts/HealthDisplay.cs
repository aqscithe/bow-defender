﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthDisplay : MonoBehaviour
{
    // cached references
    Player player;
    TextMeshProUGUI healthDisplay;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        healthDisplay = GetComponent<TextMeshProUGUI>();
        UpdateHealthDisplay();
    }

    private void UpdateHealthDisplay()
    {
        healthDisplay.text = player.GetHealth().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHealthDisplay();
    }
}
