﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShredder : MonoBehaviour
{
    //config params
    [SerializeField] float timeToDestroy = 0.1f;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject, timeToDestroy);
    }
}
