﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField] float delayTillGameOver = 3f;
    [SerializeField] float delayTillNextLevel = 5f;

    int sceneIndex;

    // cached references
    GameStatus gameStatus;
    Player player;
    GameCanvas gameCanvas;

    private void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        gameStatus = FindObjectOfType<GameStatus>();
        player = FindObjectOfType<Player>();
        gameCanvas = FindObjectOfType<GameCanvas>();
    }

    public void LoadGameOver()
    {
        StartCoroutine(Waiting());
    }

    IEnumerator Waiting()
    {
        yield return new WaitForSeconds(delayTillGameOver);
        SceneManager.LoadScene("Game Over");
    }

    public void OnLastWave()
    {
        StartCoroutine(CheckLevelForEnemies());
    }

    IEnumerator CheckLevelForEnemies()
    {
        if(FindObjectsOfType<Enemy>().Length <= 0)
        {
            yield return new WaitForSeconds(delayTillNextLevel);
            LoadNextScene();
        }
    }

    public void LoadFirstLevel()
    {
        SceneManager.LoadScene("Level 1");
        ResetGame();
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(sceneIndex + 1);
        gameStatus.ResetLevelScore();
    }

    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
        ResetGame();
    }

    private void ResetGame()
    {
        gameStatus.ResetGameStatus();
        gameCanvas.ResetGameCanvas();
        player.ResetPlayer();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
