﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupDispenser : MonoBehaviour
{
    [SerializeField][Range(0.1f, 100f)] float pointPercentageForPowerup = 70f;
    [SerializeField] int levelPointTotal; // serialized for debugging

    // cached references
    EnemySpawner enemySpawner;
    GameStatus gameStatus;
    int loopCount;


    List<WaveConfig> waveConfigs;
    int wavesToBeat;
    [SerializeField] int powerupPoints; // serialized for debugging


    // Start is called before the first frame update
    void Start()
    {
        SetupPowerupDispenser();
        CalculateTotalPossiblePoints();
        CalculatePowerupPoints();
    }


    private void SetupPowerupDispenser()
    {
        gameStatus = FindObjectOfType<GameStatus>();
        levelPointTotal = 0;
        powerupPoints = 0;
        loopCount = 0;
        enemySpawner = FindObjectOfType<EnemySpawner>();
        waveConfigs = enemySpawner.GetWaveConfigs();
        wavesToBeat = enemySpawner.GetWavesToBeat();
    }

    private void CalculatePowerupPoints()
    {
        float levelPointTotalFloat = (float)levelPointTotal;
        powerupPoints = (int)(levelPointTotalFloat * pointPercentageForPowerup * .01);
    }

    public int GetPowerupPoints()
    {
        return powerupPoints;
    }

    private void CalculateTotalPossiblePoints()
    {
        while(loopCount < wavesToBeat)
        {
            foreach (WaveConfig waveConfig in waveConfigs)
            {
                if(loopCount >= wavesToBeat)
                {
                    break;
                }
                int enemyCount = waveConfig.GetEnemyCount();
                int pointsPerEnemy = waveConfig.GetEnemyPrefab().GetComponent<Enemy>().GetScoreValue();
                levelPointTotal += (enemyCount * pointsPerEnemy);
                loopCount++;
            }
        }
        
    }
}
