﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Audio")]
    [SerializeField] AudioClip projectileFireClip;
    [Range(0f, 1f)] [SerializeField] float projectileVolume = 0.1f;


    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(projectileFireClip, Camera.main.transform.position, projectileVolume);
    }
}
