﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ScoreDisplay : MonoBehaviour
{
    // cached references
    GameStatus gameStatus;
    TextMeshProUGUI scoreDisplay;

    void Start()
    {
        gameStatus = FindObjectOfType<GameStatus>();
        scoreDisplay = GetComponent<TextMeshProUGUI>();
        UpdateScoreText();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        scoreDisplay.text = gameStatus.GetScore().ToString();
    }
}
