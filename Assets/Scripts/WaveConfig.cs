﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject pathPrefab;
    [SerializeField] float enemySpawnRate = 1f;
    [SerializeField] float spawnRandomFactor = 0.3f;
    [SerializeField] int enemyCount = 4;
    [SerializeField] float enemySpeed = 2f;


    public GameObject GetEnemyPrefab() { return enemyPrefab; }

    public List<Transform> GetWaypoints()
    {
        var waveWaypoints = new List<Transform>();
        foreach (Transform waypoint in pathPrefab.transform)
        {
            waveWaypoints.Add(waypoint);
        }
        return waveWaypoints;
    }

    public float GetEnemySpawnRate() { return enemySpawnRate; }

    public int GetEnemyCount() { return enemyCount; }

    public float GetEnemySpeed(){ return enemySpeed; }

    public float GetSpawnRandomFactor() { return spawnRandomFactor; }
}
